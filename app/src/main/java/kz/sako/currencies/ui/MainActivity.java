package kz.sako.currencies.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import kz.sako.currencies.R;
import kz.sako.currencies.adapter.CurrenciesListAdapter;
import kz.sako.currencies.database.MySqlOpenHelper;
import kz.sako.currencies.models.CurrencyModel;
import kz.sako.currencies.utils.DataManager;

import static kz.sako.currencies.CurrenciesApplication.BASE_CURRENCY;
import static kz.sako.currencies.CurrenciesApplication.DATA_BASE_CURRENCY;
import static kz.sako.currencies.CurrenciesApplication.DATA_PREFERENCES;
import static kz.sako.currencies.CurrenciesApplication.IS_CURRENCIES_LOADED;

public class MainActivity extends AppCompatActivity {
    private List<CurrencyModel> mCurrencyModelList = new ArrayList<>();
    private List<CurrencyModel> mCurrencyCheckedModelList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private CurrenciesListAdapter mCurrenciesListAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout = null;
    private MySqlOpenHelper mMySqlOpenHelper;
    private SharedPreferences mSharedPreferences, mSharedPreferencesBaseCurrency;
    private String baseCurrency;
    private double baseCurrencyPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mMySqlOpenHelper = new MySqlOpenHelper(this);

        DataManager.init(this);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.activity_main_swipe_refresh);
        mRecyclerView = (RecyclerView) findViewById(R.id.activity_main_recycler_view);

        mSharedPreferences = getApplicationContext().getSharedPreferences(DATA_PREFERENCES,
                Context.MODE_PRIVATE);
        mSharedPreferencesBaseCurrency = getApplicationContext().getSharedPreferences(DATA_BASE_CURRENCY,
                Context.MODE_PRIVATE);

        baseCurrency = mSharedPreferencesBaseCurrency.getString(BASE_CURRENCY, "USD");

        if (mSharedPreferences.getBoolean(IS_CURRENCIES_LOADED, false)) {
            mCurrencyCheckedModelList = mMySqlOpenHelper.getCheckedCurrencies();
            mCurrencyModelList = mMySqlOpenHelper.getCurrencies();
        }

        mCurrenciesListAdapter = new CurrenciesListAdapter(mCurrencyCheckedModelList);
        RecyclerView.LayoutManager layoutManager =
                new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(),
                LinearLayoutManager.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mCurrenciesListAdapter);

        if (savedInstanceState == null) {
            prepareCurrencyData();
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                prepareCurrencyData();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        prepareCurrencyData();
    }

    public void prepareCurrencyData() {
        baseCurrency = mSharedPreferencesBaseCurrency.getString(BASE_CURRENCY, "USD");
        mCurrencyCheckedModelList.clear();
        if (mSharedPreferences.getBoolean(IS_CURRENCIES_LOADED, false)) {
            mCurrencyCheckedModelList.addAll(mMySqlOpenHelper.getCheckedCurrencies());
        }
        if (baseCurrency.equals("USD")) {
            for (int i = 0; i < mCurrencyCheckedModelList.size(); i++) {
                if (!mCurrencyCheckedModelList.get(i).getName().contains("USD")) {
                    String currencyName = mCurrencyCheckedModelList.get(i).getName();
                    mCurrencyCheckedModelList.get(i).setName("USD/" + currencyName);
                }
            }
        } else {
            for (int i = 0; i < mCurrencyModelList.size(); i++) {
                if (mCurrencyModelList.get(i).getName().contains(baseCurrency)) {
                    baseCurrencyPrice = mCurrencyModelList.get(i).getPrice();
                }
            }
            for (int i = 0; i < mCurrencyCheckedModelList.size(); i++) {
                double currentPrice = mCurrencyCheckedModelList.get(i).getPrice();
                double newPrice = currentPrice / baseCurrencyPrice;
                mCurrencyCheckedModelList.get(i).setPrice(newPrice);
                if (mCurrencyCheckedModelList.get(i).getName().contains("/")) {
                    mCurrencyCheckedModelList.get(i).setName(baseCurrency +
                            mCurrencyCheckedModelList.get(i).getName().substring(3));
                } else {
                    mCurrencyCheckedModelList.get(i).setName(baseCurrency + "/" +
                            mCurrencyCheckedModelList.get(i).getName());
                }
            }
        }
        mCurrenciesListAdapter.notifyDataSetChanged();
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

}
