package kz.sako.currencies.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import kz.sako.currencies.R;
import kz.sako.currencies.database.MySqlOpenHelper;
import kz.sako.currencies.models.CurrencyModel;

import static kz.sako.currencies.CurrenciesApplication.BASE_CURRENCY;
import static kz.sako.currencies.CurrenciesApplication.DATA_BASE_CURRENCY;

/**
 * Created by Sayazhan on 07.09.2017.
 * Hala Madrid!
 */

public class SettingsActivity extends AppCompatActivity {
    private LinearLayout mChooseCurrenciesLL, mBaseCurrencyLL;
    private TextView mBaseCurrencyTv;
    private List<CurrencyModel> mCurrencyModelList, mBaseCurrencyList;
    private MySqlOpenHelper mMySqlOpenHelper;
    private SharedPreferences mSharedPreferences;
    private String baseCurrency;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mSharedPreferences = getApplicationContext().getSharedPreferences(DATA_BASE_CURRENCY,
                Context.MODE_PRIVATE);
        mMySqlOpenHelper = new MySqlOpenHelper(this);

        mChooseCurrenciesLL = (LinearLayout) findViewById(R.id.activity_settings_choose_currencies_ll);
        mBaseCurrencyLL = (LinearLayout) findViewById(R.id.activity_settings_base_currency_ll);
        mBaseCurrencyTv = (TextView) findViewById(R.id.activity_settings_base_currency_tv);
        baseCurrency = mSharedPreferences.getString(BASE_CURRENCY, "USD");
        mBaseCurrencyTv.setText(baseCurrency);

        mBaseCurrencyLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectBaseCurrency();
            }
        });
        mChooseCurrenciesLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseCurrencies();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void selectBaseCurrency() {
        mBaseCurrencyList = mMySqlOpenHelper.getCurrencies();
        Collections.sort(mBaseCurrencyList, new Comparator<CurrencyModel>() {
            @Override
            public int compare(CurrencyModel o1, CurrencyModel o2) {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
        final String names[] = new String[mBaseCurrencyList.size()];
        for (int i = 0; i < names.length; i++) {
            if (mBaseCurrencyList.get(i).getName().contains("/")) {
                        names[i] = mBaseCurrencyList.get(i).getName().substring(4);
            } else {
                names[i] = mBaseCurrencyList.get(i).getName();
            }
        }
        int checkedCurrency = 0;
        for (int i = 0; i < names.length; i++) {
            if (names[i].equals(baseCurrency)) {
                checkedCurrency = i;
            }
        }

        builder.setSingleChoiceItems(names, checkedCurrency, null);
        builder.setCancelable(false);
        builder.setTitle("Choose Base Currency.");

        builder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                mBaseCurrencyTv.setText(names[selectedPosition]);
                baseCurrency = names[selectedPosition];
                SharedPreferences.Editor editor = mSharedPreferences.edit();
                editor.putString(BASE_CURRENCY, names[selectedPosition]);
                editor.apply();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void chooseCurrencies() {
        mCurrencyModelList = mMySqlOpenHelper.getCurrencies();
        Collections.sort(mCurrencyModelList, new Comparator<CurrencyModel>() {
            @Override
            public int compare(CurrencyModel o1, CurrencyModel o2) {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });
        AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
        final String names[] = new String[mCurrencyModelList.size()];
        final String namesByBaseCurrency[] = new String[mCurrencyModelList.size()];
        for (int i = 0; i < names.length; i++) {
            names[i] = mCurrencyModelList.get(i).getName();
        }
        for (int i = 0; i < namesByBaseCurrency.length; i++) {
            if (mCurrencyModelList.get(i).getName().contains("/")) {
                namesByBaseCurrency[i] = baseCurrency + "/" + mCurrencyModelList.get(i).getName().substring(4);
            } else {
                namesByBaseCurrency[i] = baseCurrency + "/" + mCurrencyModelList.get(i).getName();
            }
        }
        boolean[] checkedNames = new boolean[mCurrencyModelList.size()];
        for (int i = 0; i < checkedNames.length; i++) {
            if (mCurrencyModelList.get(i).getChecked() == 1) {
                checkedNames[i] = true;
            }
        }

        builder.setMultiChoiceItems(namesByBaseCurrency, checkedNames, null);

        builder.setCancelable(false);
        builder.setTitle("Choose Multiple Currencies.");
        builder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                SparseBooleanArray sbArray = ((AlertDialog) dialog).getListView().getCheckedItemPositions();
                for (int i = 0; i < sbArray.size(); i++) {
                    int key = sbArray.keyAt(i);
                    mMySqlOpenHelper.setCurrencyChecked(names[key], sbArray.get(key));
                }
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
