package kz.sako.currencies;

import android.app.Application;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import kz.sako.currencies.libs.VolleySingleton;

/**
 * Created by Sayazhan on 07.09.2017.
 * Hala Madrid!
 */

public class CurrenciesApplication extends Application {
    private static RequestQueue requestQueue;
    public static String baseCurrency = "";

    public static final String DATA_PREFERENCES = "data_preferences";
    public static final String IS_CURRENCIES_LOADED = "is_currencies_loaded";
    public static final String DATA_BASE_CURRENCY = "data_base_currency";
    public static final String BASE_CURRENCY = "base_currency";

    @Override
    public void onCreate() {
        super.onCreate();
        if (requestQueue == null) {
            Volley.newRequestQueue(CurrenciesApplication.this);
            requestQueue = VolleySingleton.getInstance(CurrenciesApplication.this).getRequestQueue();
        }

    }

    public static RequestQueue getRequestQueue() {
        return requestQueue;
    }

    public static <T> Request<T> addRequest(Request<T> request) {
        request.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        return getRequestQueue().add(request);
    }
}
