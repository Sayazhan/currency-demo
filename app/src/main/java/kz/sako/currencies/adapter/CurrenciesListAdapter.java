package kz.sako.currencies.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import kz.sako.currencies.R;
import kz.sako.currencies.models.CurrencyModel;

/**
 * Created by Sayazhan on 07.09.2017.
 * Hala Madrid!
 */

public class CurrenciesListAdapter extends RecyclerView.Adapter<CurrenciesListAdapter.CurrenciesViewHolder> {
    private List<CurrencyModel> mCurrenciesList;

    public CurrenciesListAdapter(List<CurrencyModel> currenciesList) {
        this.mCurrenciesList = currenciesList;
        notifyDataSetChanged();
    }

    public class CurrenciesViewHolder extends RecyclerView.ViewHolder {
        private TextView currenciesName, currenciesPrice;

        public CurrenciesViewHolder(View view) {
            super(view);
            currenciesName = (TextView) view.findViewById(R.id.currencies_list_name);
            currenciesPrice = (TextView) view.findViewById(R.id.currencies_list_price);
        }

        public void onBind(CurrencyModel currencyModel) {
            currenciesName.setText(currencyModel.getName());
            String price = new DecimalFormat("##.###",
                    new DecimalFormatSymbols(Locale.US)).format(currencyModel.getPrice());

            currenciesPrice.setText(price);
        }
    }

    @Override
    public CurrenciesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.currencies_list, parent, false);

        return new CurrenciesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CurrenciesViewHolder holder, int position) {
        CurrencyModel currencyModel = mCurrenciesList.get(position);
        holder.onBind(currencyModel);
    }

    @Override
    public int getItemCount() {
        return mCurrenciesList.size();
    }
}
