package kz.sako.currencies.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import kz.sako.currencies.models.CurrencyModel;

import static kz.sako.currencies.models.CurrencyModel.parseCursor;

/**
 * Created by Sayazhan on 09.09.2017.
 * Hala Madrid!
 */
public class MySqlOpenHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "currency_db";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_CURRENCY = "currency";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_PRICE = "price";
    private static final String COLUMN_CHECKED = "checked";

    public MySqlOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_CURRENCY + " ( "
                + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_NAME + " VARCHAR, "
                + COLUMN_PRICE + " REAL, "
                + COLUMN_CHECKED + " INTEGER " + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CURRENCY);
        onCreate(db);
    }

    public void insertCurrencies(List<CurrencyModel> currencies) {
        SQLiteDatabase database = getWritableDatabase();
        database.beginTransaction();
        try {
            for (int i = 0; i < currencies.size(); i++) {
                CurrencyModel currency = currencies.get(i);
                ContentValues contentValues = new ContentValues();
                contentValues.put(COLUMN_NAME, currency.getName());
                contentValues.put(COLUMN_PRICE, currency.getPrice());
                database.insert(TABLE_CURRENCY, null, contentValues);
            }
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }
    }

    public List<CurrencyModel> getCurrencies() {
        SQLiteDatabase database = getReadableDatabase();
        List<CurrencyModel> currencies = new ArrayList<>();
        Cursor cursor = database.query(TABLE_CURRENCY, new String[]{COLUMN_ID, COLUMN_NAME, COLUMN_PRICE, COLUMN_CHECKED},
                null, null, null, null, null);
        cursor.moveToFirst();
        do {
            currencies.add(parseCursor(cursor));
        } while(cursor.moveToNext());

        return currencies;
    }
    public List<CurrencyModel> getCheckedCurrencies() {
        SQLiteDatabase database = getReadableDatabase();
        List<CurrencyModel> currencies = new ArrayList<>();
        Cursor cursor = database.query(TABLE_CURRENCY, new String[]{COLUMN_ID, COLUMN_NAME, COLUMN_PRICE, COLUMN_CHECKED},
                null, null, null, null, null);
        cursor.moveToFirst();
        do {
            if(parseCursor(cursor).getChecked()==1){
                currencies.add(parseCursor(cursor));
            }
        } while(cursor.moveToNext());

        return currencies;
    }

    public void setCurrencyChecked(String name, boolean checked) {
        SQLiteDatabase database = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_CHECKED, checked?1:0);
        database.update(TABLE_CURRENCY, contentValues, COLUMN_NAME + "=" + "\""+name+"\"", null);
    }
}
