package kz.sako.currencies.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import kz.sako.currencies.database.MySqlOpenHelper;
import kz.sako.currencies.libs.VolleySingleton;
import kz.sako.currencies.models.CurrencyModel;

import static kz.sako.currencies.CurrenciesApplication.DATA_PREFERENCES;
import static kz.sako.currencies.CurrenciesApplication.IS_CURRENCIES_LOADED;

/**
 * Created by Sayazhan on 07.09.2017.
 * Hala Madrid!
 */

public class DataManager {

    private static DataManager sInstance;
    private final SharedPreferences mSharedPreferences;
    private final RequestQueue mRequestQueue;
    private MySqlOpenHelper mMySqlOpenHelper;

    public static void init(Context context) {
        sInstance = new DataManager(context);
    }

    public static DataManager getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException(DataManager.class.getSimpleName()
                    + " is not initialized yet");
        }
        return sInstance;
    }

    private DataManager(Context context) {

        mMySqlOpenHelper= new MySqlOpenHelper(context);
        mSharedPreferences = context.getApplicationContext().getSharedPreferences(DATA_PREFERENCES,
                Context.MODE_PRIVATE);
        mRequestQueue = VolleySingleton.getInstance(context).getRequestQueue();

        if (!mSharedPreferences.getBoolean(IS_CURRENCIES_LOADED, false)) {
            downloadCurrencies();
        }
    }

    private void downloadCurrencies() {
        final Request<JSONObject> request = new JsonObjectRequest("https://finance.yahoo.com/webservice/v1/" +
                "symbols/allcurrencies/quote?format=json", null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                JSONObject jsonObjectList;
                JSONArray jsonArrayResources;
                try {
                    jsonObjectList = response.getJSONObject("list");
                    jsonArrayResources = jsonObjectList.getJSONArray("resources");
                    List<CurrencyModel> list = new ArrayList<>();
                    for (int i = 0; i < jsonArrayResources.length(); i++) {
                        JSONObject jsonObject = jsonArrayResources.getJSONObject(i);
                        JSONObject jsonObject1 = jsonObject.getJSONObject("resource");
                        JSONObject jsonObject2 = jsonObject1.getJSONObject("fields");
                        CurrencyModel currencies = CurrencyModel.parseJson(jsonObject2);
                        if (currencies != null) {
                            list.add(currencies);
                        }
                    }
                    mMySqlOpenHelper.insertCurrencies(list);
                    onCurrenciesLoaded();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }

    private void onCurrenciesLoaded(){
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(IS_CURRENCIES_LOADED, true);
        editor.apply();
    }

}
