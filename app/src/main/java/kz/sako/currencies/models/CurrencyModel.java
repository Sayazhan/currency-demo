package kz.sako.currencies.models;

import android.database.Cursor;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sayazhan on 07.09.2017.
 * Hala Madrid!
 */

public class CurrencyModel {
    private String mName;
    private double mPrice;
    private int mChecked;

    public static CurrencyModel parseJson(JSONObject jsonObject) {
        CurrencyModel currencyModel = null;
        try {
            String name = jsonObject.getString("name");
            String price = jsonObject.getString("price");

            currencyModel = new CurrencyModel(name, Double.parseDouble(price), 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return currencyModel;
    }

    public CurrencyModel(String name, double price, int checked) {
        mName = name;
        mPrice = price;
        mChecked = checked;
    }

    public void setName(String name) {
        mName = name;
    }

    public void setPrice(double price) {
        mPrice = price;
    }

    public void setChecked(int checked) {
        mChecked = checked;
    }

    public String getName() {
        return mName;
    }

    public double getPrice() {
        return mPrice;
    }

    public int getChecked() {
        return mChecked;
    }

    public static CurrencyModel parseCursor(Cursor cursor) {
        String name = cursor.getString(1);
        double price = cursor.getDouble(2);
        int checked = cursor.getInt(3);

        return new CurrencyModel(name, price, checked);
    }
}
